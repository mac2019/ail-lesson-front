# ail-lesson

#### 介绍
在线课程教育系统，提供在线课程，在线观看，订单，购买，支付等前台功能；后台管理系统：课程管理，课程分类管理，订单管理，营销管理，讲师管理，课程统计，公众号管理，视频点播，视频直播等；本系统使用框架比较新，可以兼容一些新特性，满足一些编程爱好者对新技术使用，同时使用本系统，进行二次开发，免费使用，欢迎有需求的朋友们拿走不留，记着点个星星哈！！！！

#### 软件架构
软件架构：Vue + ElementUI

软件目录：

![输入图片说明](image.png)


#### 安装教程

1.  安装nodejs,webstorm


#### 使用说明

```bash
# clone the project
git clone https://gitee.com/mac2019/ail-lesson-front.git

# enter the project directory
cd ail-lesson-front

# install dependency
npm install

# develop
npm run dev
```

打开浏览器输入 http://localhost:9528

构建：

```bash
# build for test environment
npm run build:stage

# build for production environment
npm run build:prod
```


#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1. 访问CSDN博客 (https://blog.csdn.net/qq_23044533/article/details/143989763)
2. 有需要进一步帮忙二次开发的请加我微信：wx_mac2013

#### 最后

开源不易，请道友打赏一点点，支持我不断开源：

![输入图片说明](yy2024115209.jpg)
