import request from "@/utils/request";

const api_path = '/admin/order/couponInfo'

export default {
  pageList(page, limit, searchObj){
    searchObj.pageNo = page
    searchObj.pageSize = limit
    return request({
      url: `${api_path}/list`,
      method: 'post',
      data: searchObj
    })
  },
  removeById(id){
    return request({
      url: `${api_path}/delete`,
      method: 'delete',
      data: [id]
    })
  },
  save(course){
    return request({
      url: `${api_path}/add`,
      method: 'post',
      data: course
    })
  },
  getById(id){
    return request({
      url: `${api_path}/info/${id}`,
      method: 'get',
    })
  },
  updateById(course) {
    return request({
      url: `${api_path}/update`,
      method: 'put',
      data: course
    })
  },

  pageUseCouponList(page, limit, searchObj){
    searchObj.pageNo = page
    searchObj.pageSize = limit
    return request({
      url: `/order/couponUse/list`,
      method: 'post',
      data: searchObj
    })
  },
}

