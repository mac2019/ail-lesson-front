import request from "@/utils/request";

const api_path = '/admin/order/orderInfo'

export default {
  pageList(page, limit, searchObj){
    searchObj.pageNo = page
    searchObj.pageSize = limit
    return request({
      url: `${api_path}/list`,
      method: 'post',
      data: searchObj
    })
  },
}

