import request from "@/utils/request";

const api_path = '/admin/video/course'
export default {
  pageCourseList(page, limit, searchObj){
    searchObj.pageNo = page
    searchObj.pageSize = limit
    return request({
      url: `${api_path}/list`,
      method: 'post',
      data: searchObj
    })
  },
  removeCourseById(id){
    return request({
      url: `${api_path}/delete`,
      method: 'delete',
      data: [id]
    })
  },
  saveCourse(course){
    return request({
      url: `${api_path}/add`,
      method: 'post',
      data: course
    })
  },
  getCourseById(id){
    return request({
      url: `${api_path}/info/${id}`,
      method: 'get',
    })
  },
  updateCourse(course) {
    return request({
      url: `${api_path}/update`,
      method: 'put',
      data: course
    })
  },
  publishCourse(id) {
    return request({
      url: `${api_path}/publish/${id}`,
      method: 'put',
    })
  },
  getCoursePublishById(id) {
    return request({
      url: `${api_path}/publish/query/${id}`,
      method: 'get',
    })
  },
  batchRemoveCourse(idList) {
    return request({
      url: `${api_path}/delete`,
      method: `delete`,
      data: idList
    })
  },
}
