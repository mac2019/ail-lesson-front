import request from "@/utils/request";

const api_path = '/admin/video/chapter'
export default {
  getNestedTreeList(courseId) {
    return request({
      url: `${api_path}/getNestedTreeList/${courseId}`,
      method: 'get'
    })
  },
  removeChapterById(id){
    return request({
      url: `${api_path}/delete`,
      method: 'delete',
      data: [id]
    })
  },
  saveChapter(chapter){
    return request({
      url: `${api_path}/add`,
      method: 'post',
      data: chapter
    })
  },
  getChapterById(id){
    return request({
      url: `${api_path}/info/${id}`,
      method: 'get',
    })
  },
  updateChapter(chapter) {
    return request({
      url: `${api_path}/update`,
      method: 'put',
      data: chapter
    })
  }
}
