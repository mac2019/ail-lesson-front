import request from "@/utils/request";

const api_path = '/admin/vid/teacher'
export default {
  pageList(page, limit, searchObj){
    searchObj.pageNo = page
    searchObj.pageSize = limit
     return request({
       url: `${api_path}/list`,
       method: 'post',
       data: searchObj
     })
  },
  removeTeacherId(id){
    return request({
      url: `${api_path}/delete`,
      method: 'delete',
      data: [id]
    })
  },
  saveTeacher(teacher){
    return request({
      url: `${api_path}/add`,
      method: 'post',
      data: teacher
    })
  },
  getTeacherById(id){
    return request({
      url: `${api_path}/info/${id}`,
      method: 'get',
    })
  },
  updateTeacher(teacher) {
    return request({
      url: `${api_path}/update`,
      method: 'put',
      data: teacher
    })
  },
  batchRemove(idList) {
    return request({
      url: `${api_path}/delete`,
      method: `delete`,
      data: idList
    })
  },
}
