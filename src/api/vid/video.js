import request from '@/utils/request'

const api_path = '/admin/vid/video'

export default {

  removeVideoById(id){
    return request({
      url: `${api_path}/delete`,
      method: 'delete',
      data: [id]
    })
  },
  saveVideo(video){
    return request({
      url: `${api_path}/add`,
      method: 'post',
      data: video
    })
  },
  getVideoById(id){
    return request({
      url: `${api_path}/info/${id}`,
      method: 'get',
    })
  },
  updateVideo(video) {
    return request({
      url: `${api_path}/update`,
      method: 'put',
      data: video
    })
  }
}
