import request from '@/utils/request'

const api_path = '/admin/video/video-visitor'

export default {
  findCount(courseId, startDate, endDate) {
    return request({
      url: `${api_path}/queryCount/${courseId}/${startDate}/${endDate}`,
      method: 'get'
    })
  }
}
