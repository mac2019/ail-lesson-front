import request from "@/utils/request";

const api_path = '/admin/video/subject'
export default {
  pageSubjectList(page, limit, searchObj){
    searchObj.pageNo = page
    searchObj.pageSize = limit
    return request({
      url: `${api_path}/list`,
      method: 'post',
      data: searchObj
    })
  },
  removeSubjectId(id){
    return request({
      url: `${api_path}/delete`,
      method: 'delete',
      data: [id]
    })
  },
  saveSubject(subject){
    return request({
      url: `${api_path}/add`,
      method: 'post',
      data: subject
    })
  },
  getSubjectById(id){
    return request({
      url: `${api_path}/info/${id}`,
      method: 'get',
    })
  },
  updateSubject(subject) {
    return request({
      url: `${api_path}/update`,
      method: 'put',
      data: subject
    })
  },
  batchSubjectRemove(idList) {
    return request({
      url: `${api_path}/delete`,
      method: `delete`,
      data: idList
    })
  },
  getChildList(id) {
    return request({
      url: `${api_path}/getChildSubject/${id}`,
      method: 'get'
    })
  }
}
